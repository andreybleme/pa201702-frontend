'use strict';

angular.module('pa201702frontendApp')
  .service('EmpresaService', ['$http', function ($http) {

  	// Constants
  	var service = this;

  	var API_ENDPOINT = 'http://localhost:8080';
  	var EMPRESA_ENDPOINT = "/empresa";

  	// Actions
    service.cadastrarEmpresa = cadastrarEmpresa;

    function cadastrarEmpresa(nome, email) {
    	var empresa = {
    		"nome": nome,
    		"email": email
    	}
    	
    	return $http({ 
    	 	method: 'POST',
			url: API_ENDPOINT + EMPRESA_ENDPOINT,
			data: empresa
		});
    }

  }]);
