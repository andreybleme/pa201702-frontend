'use strict';

angular.module('pa201702frontendApp')
  .controller('EmpresaCtrl', ['EmpresaService', function (empresaService) {

  	var vm = this;

  	// Models
  	vm.model = {};
  	vm.model.nome = "";
  	vm.model.email = "";
  	vm.model.mensagemSucesso;
  	vm.model.mensagemErro;

  	// Actions
    vm.cadastrarEmpresa = cadastrarEmpresa;

    function cadastrarEmpresa() {
    	empresaService.cadastrarEmpresa(vm.model.nome, vm.model.email).then(function (response) {
    		vm.model.mensagemSucesso = true;
    	}, function (response) {
    		vm.model.mensagemErro = true;
    	});
    }
    
  }]);
