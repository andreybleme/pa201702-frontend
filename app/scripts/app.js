'use strict';

/**
 * Main module of the application.
 */
angular
  .module('pa201702frontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'mainCtrl'
      })
      .when('/cadastrar-empresa', {
        templateUrl: 'views/cadastrar-empresa.html',
        controller: 'EmpresaCtrl',
        controllerAs: 'empresaCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
